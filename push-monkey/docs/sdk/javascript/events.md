#Events for Segmentation

Events are base building blocks for segmenting your audience. 

##Creating events automatically

Events are automatically created when the Push Monkey JavaScript SDK is called.

The following parameters are important for an event:

Parameter | Type | Description
--- | --- | ---
category|string| **Required** The "category" of the event tracked.
object | object | **Optional** If you want to send some aditional info with the tracked event. The required key name is "value".

To track an event, add the following script on the page or page action you want to 
track. You can track as many events as you need on a page.

###Example 1

    window.push_monkey_sdk.trackEvent('booked-flight');

In this example, the 'booked-flight' event is tracked.

###Example 2

    window.push_monkey_sdk.trackEvent('coupon-redeemed', {'value': 'pizza'});

In this example, the 'coupon-redeemed' event is tracked and it contains as an aditional value the string 'pizza'.

