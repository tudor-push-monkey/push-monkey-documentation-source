#Manually Asking For Push Notifications Permission

Sometimes automatically asking the user to subscribe for push notification on page load is too early and can be ineffective. As an alternative, the visitor can be asked to grant the permission to receive push notification after clicking on a link or button by calling a JavaScript function.

To trigger the permission dialog, please call the following line on the desired JavaScript event:

```
window._pushmonkey.manualStart();
```

##Examples

  1. Assigned to the `click` event of a link with JavaScript

```
var specialLink = document.getElementById("special-link");
saveLink.onclick = function(){

  window._pushmonkey.manualStart();
};
```

  2. In-line assignment for `<a>` tag.

```
<a href="#" onclick="window._pushmonkey.manualStart()">Special Link</a>
```

For other options of asking the user for push notifications permissions, please see [this How To](https://intercom.help/push-monkey/how-to-s/how-to-manully-ask-visitors-to-subscribe)