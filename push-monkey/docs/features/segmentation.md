#Audience Segmentation

##What is a Segment?

Segments give you the ability to target users with more personalized and more engaging push notifications by dividing your total user base into specific audiences based on attributes like activity, location, and interests.

##How to create a Segment?

Segments are created from the "Segments" page on your Dashboard on [getpushmonkey.com](https://getpushmonkey.com), by clicking on the orange "Add Segment" button.

![Dashboard Navigation](https://api.monosnap.com/rpc/file/download?id=4dNyF8eOt7XXfxvlT3xLgaeqP5G5o2)

You should now see the the form to add a Segment that looks like this:

![Add Segment](https://api.monosnap.com/rpc/file/download?id=VyOUDZ6q39hh2YQ04s8NSNs3rZH0TI)

This form contains a few important fields:

1. The title of the segment is for your internal reference only. Please enter
something relevant and easy to recognize later.
2. The list of filters currently added to a Segment. When you start, this is empty.
3. A list of [events](??) that you can add filters for.
4. Once you're happy with your filters, click on the green "Save Segment" button.

##What are filters?

Filters are components of a Segment help narrowing down your audience. 
A Segment can have one or more filters.

Filters are based on the events that you are tracking using the [Push Monkey JavaScript SDK](/sdk/javascript/events.md) on your website. Each tracked event can be added as a filter. 

By default, all filter can track either the value of event, the time when it happened or the number of occurences.

###Example

Let's say that you want to create a segment that includes all the subscribers that have booked a flight on your website. To do so, you would first need to track the 'Booked Flight' event using the [JavaScript SDK](/sdk/javascript/events.md). Once that is done, go to the Segments page of your Dashboard and click "Add Segment".

The list of possible filters should now include this "Booked Flight" event. Click on it and decide what exact behaviour you would like to include in this segment.

##How to create and track custom events

Events can be created in a 2 ways: 

 * automatically when calling the JavaScript SDK. See more info [here](/sdk/javascript/events.md)
 * manually from the Dashboard


##Creating events manually

From the "Segments" page of your Dashboard, click on the "Events" button on the top right to get to the list of events. No click on the orange "Add Event" button on the top right.

![Events](https://api.monosnap.com/rpc/file/download?id=FpGF2CQm2qn1FV9z0ZDOUyMX1RPRNY)

The page to add a new event contains a few fields, each with instructions under them. The most important and only required field is the **"Label"**, from which the "Category" will be auto-generated. This category will later be used in the JavaScript code to track this event.











