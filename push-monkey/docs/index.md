##Product Overview
**About Push Monkey and how to get started**


Send push notifications directly to your readers’ mobiles and desktops when new content is fresh from the oven. We support all major native and mobile platforms and provide online dashboard for marketers to design and send push notifications.

We offer platform specific plugins for WordPress, Shopify, WooCommerce and Magento.