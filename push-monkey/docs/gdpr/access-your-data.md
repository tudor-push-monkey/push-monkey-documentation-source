#Access or Modify your Data

As required by the GDPR, users have access to their data at any point of time and/or even modify it, if required.

Incase you wish to gain access to your data for a website that you are currently subscribed to, here are the steps:

- Extract your Subscription Identifier. [Click here](/here) to find out how.
- Send the key to [support@getpushmonkey.com](mailto:support@getpushmonkey.com)
- Our team would get in touch with you via email with all the data that is stored against that Subscription Identifier.
- If you wish to modify any data, you can revert to the same mail with your requirements.