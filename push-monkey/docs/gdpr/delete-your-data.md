#Delete you Data

Under GDPR (General Data Protection Regulation), users in the EU (European Region) would have full control over their data and can delete, demand access and even modify when required.

If you are subscribed to an Push Monkey powered website, you can remove your data easily from our database as and when required by simply unsubscribing from notifications. Upon unsubscription, all data is removed from our database. !!??

For businesses using Push Monkey, when you cancel your account, all the profile data associated with your account gets deleted permanently. We are required by law to keep some transaction data for accounting reasons.

##How to Unsubscribe

For exact steps, please see [this link](https://intercom.help/push-monkey/how-to-s/how-to-unsubscribe-from-web-push-notifications).

##Further details

For more info about our commitment to follow the GDPR, please read [our GDPR statement](http://getpushmonkey.com/gdpr?source=docs)



